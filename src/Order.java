import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Scanner;

public class Order {

    private static int orderNumber = 10000;
    private int clientNumber;
    private LocalTime orderTime = LocalTime.now();
    private ArrayList<Pizza> pizza = new ArrayList<Pizza>();
    public final String[] ARRAYOFINGREDIENTS = {"Tomato Paste", "Cheese", "Salami", "Bacon", "Garlic",
            "Corn", "Pepperoni", "Olives"};
    public final double[] ARRAYOFINGREDIENTSPRICES = {1, 1, 1.5, 1.2, 0.3, 0.7, 0.6, 0.5};

    public Order(int clientNumber) {
        System.out.println("Welcome to the 'Palmetto' Pizzeria! A new order is being processed.");
        this.clientNumber = clientNumber;
        orderNumber++;
    }

    public boolean isRightPizzaName(String pizzaName) {
        return ((pizzaName.length() >= 4) && (pizzaName.length() <= 20) && (pizzaName != null) &&
                (!pizzaName.equals("")) && (pizzaName.matches("^[a-zA-Z]*$")));
    }

    public boolean isNumeric(String inputValue) {
        if (inputValue == null) {
            return false;
        }
        try {
            int number = Integer.parseInt(inputValue);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public void addPizza(String pizzaName, int pizzaType, int pizzaAmount) {
        System.out.println("Start creating a new pizza:");
        if (pizzaAmount > 10) {
            System.out.println("Sorry, but the number of pizzas should not exceed 10! Amount will be 10 by default.\n" +
                    "If you are not satisfied with this condition and want to cancel the current pizza, select \"Delete pizza\":");
            pizzaAmount = 10;
        }
        if (!isRightPizzaName(pizzaName)) {
            System.out.println("Sorry, but pizza name '" + pizzaName + "' is invalid. Name will be changed to " + Integer.toString(clientNumber) + "_" + (pizza.size() + 1));
            pizzaName = Integer.toString(clientNumber) + "_" + (pizza.size() + 1);
        }
        pizza.add(new Pizza(pizzaName, pizzaType, pizzaAmount));
        Scanner input = new Scanner(System.in);
        String inputValue = "";
        while (!inputValue.equals("3") && !inputValue.equals("4")) {
            System.out.println("Which option would you like to choose?");
            System.out.println("1 - Change attributes\n2 - Add/Remove ingredients\n3 - Delete pizza\n4 - Finish");
            inputValue = input.nextLine();
            switch (inputValue) {
                case "1":
                    System.out.println("Current attributes of your pizza: ");
                    showPizzaAttributes(pizza.size() - 1);
                    System.out.println("Which attribute would you like to change?\n1 - Name\n2 - Pizza type\n3 - Amount of pizza"
                            + "\n0 - Exit from change settings");
                    String inputCommand = input.nextLine();
                    changePizzaAttributes(pizza.size() - 1, inputCommand);
                    break;
                case "3":
                    pizza.remove(pizza.size() - 1);
                    System.out.println("Pizza was deleted");
                    return;
                case "2":
                    System.out.println("Would you like to add or remove ingredients?\n1 - Add\n2 - Remove");
                    String changedAttribute = input.nextLine();
                    if (changedAttribute.equals("1")) {
                        addIngredient(pizza.size() - 1);
                    } else if (changedAttribute.equals("2")) {
                        removeIngredient(pizza.size() - 1);
                    } else {
                        System.out.println("Invalid command.");
                    }
                    break;
                case "4":
                    System.out.println("You finished making your pizza.\n");
                    break;
                default:
                    System.out.println("You entered invalid command. Try again.\n");
            }
        }
    }

    public void changePizzaAttributes(int pizzaIndex, String commandNumber) {
        Scanner input = new Scanner(System.in);
        String changedAttribute = "";
        switch (commandNumber) {
            case "1":
                System.out.println("Please enter new name for your pizza:");
                changedAttribute = input.nextLine();
                if (!isRightPizzaName(changedAttribute)) {
                    System.out.println("Sorry, but pizza name '" + changedAttribute + "' is invalid. Name will be changed to " + Integer.toString(clientNumber) + "_" + (pizzaIndex + 1));
                    changedAttribute = Integer.toString(clientNumber) + "_" + (pizzaIndex + 1);
                }
                pizza.get(pizzaIndex).setPizzaName(changedAttribute);
                break;
            case "2":
                System.out.println("Please enter new type for your pizza:\n1 - Base\n2 - Calzone");
                changedAttribute = input.nextLine();
                if (changedAttribute.equals("1")) {
                    pizza.get(pizzaIndex).setPizzaType(0);
                } else if (changedAttribute.equals(("2"))) {
                    pizza.get(pizzaIndex).setPizzaType(1);
                } else {
                    System.out.println("Invalid command.");
                }
                break;
            case "3":
                System.out.println("Please enter new amount for your pizza:");
                changedAttribute = input.nextLine();
                if (isNumeric(changedAttribute)) {
                    if (Integer.parseInt(changedAttribute) < 1 || Integer.parseInt(changedAttribute) > 10) {
                        System.out.println("Amount can't be lower than 1 and higher than 10");
                    } else {
                        pizza.get(pizzaIndex).setPizzaAmount(Integer.parseInt(changedAttribute));
                    }
                }
                break;
            case "0":
                System.out.println("Exit from change settings.");
                break;
            default:
                System.out.println("Invalid command.");
        }
    }

    public int getAmountOfIngredients(int indexOfCurrentPizza) {
        int amountOfIngredients = 0;
        for (int i = 0; i < 8; i++) {
            if (pizza.get(indexOfCurrentPizza).getPizzaIngredients()[i] == 1) {
                amountOfIngredients++;
            }
        }
        return amountOfIngredients;
    }

    public void removeIngredient(int pizzaIndex) {
        if (getAmountOfIngredients(pizzaIndex) == 0) {
            System.out.println("List of ingredients is empty.");
            return;
        }
        System.out.println("Which ingredients would you like to remove:");
        Scanner input = new Scanner(System.in);
        String inputValue = "";
        while (!inputValue.equals("0")) {
            showListOfCurrentIngredients(pizzaIndex);
            System.out.println("What ingredient would you like to remove from the pizza? Choose number of the " +
                    "ingredient or enter '0' to stop: ");
            inputValue = input.nextLine();
            if (inputValue.equals("0")) {
                break;
            } else if (isNumeric(inputValue) && !"".equals(inputValue)) {
                removeIngredient(inputValue, pizzaIndex);
            } else {
                System.out.println("Sorry, but you entered an invalid number");
            }
            if (getAmountOfIngredients(pizzaIndex) == 0) {
                System.out.println("List of ingredients is empty.");
                return;
            }
        }
        System.out.println("Removing operation is finished.");
    }

    public void addIngredient(int pizzaIndex) {
        if (getAmountOfIngredients(pizzaIndex) == 7) {
            System.out.println("List of ingredients is full");
            return;
        }
        System.out.println("You can have a maximum of 7 ingredients in one pizza:");
        Scanner input = new Scanner(System.in);
        String inputValue = "";
        while (!inputValue.equals("0")) {
            showListOfIngredients(pizzaIndex);
            System.out.println("What ingredient would you like to add to the pizza? Choose number of the " +
                    "ingredient or enter '0' to stop: ");
            inputValue = input.nextLine();
            if (inputValue.equals("0")) {
                break;
            } else if (isNumeric(inputValue) && !"".equals(inputValue)) {
                addIngredient(inputValue, pizzaIndex);
            } else {
                System.out.println("Sorry, but you entered an invalid number");
            }
            if (getAmountOfIngredients(pizzaIndex) == 7) {
                System.out.println("List of ingredients is full");
                return;
            }
        }
        System.out.println("You have finished adding ingredients.");
    }

    public void addIngredient(String inputValue, int indexOfCurrentPizza) {
        int ingredientNumber = Integer.parseInt(inputValue);
        for (int i = 0; i < 8; i++) {
            if (pizza.get(indexOfCurrentPizza).getPizzaIngredients()[i] == 0) {
                ingredientNumber--;
            }
            if (ingredientNumber == 0) {
                pizza.get(indexOfCurrentPizza).getPizzaIngredients()[i] = 1;
                return;
            }
        }
        System.out.println("You entered number not from the list. Please repeat command.");
    }

    public void removeIngredient(String inputValue, int indexOfCurrentPizza) {
        int ingredientNumber = Integer.parseInt(inputValue);
        for (int i = 0; i < 8; i++) {
            if (pizza.get(indexOfCurrentPizza).getPizzaIngredients()[i] == 1) {
                ingredientNumber--;
            }
            if (ingredientNumber == 0) {
                pizza.get(indexOfCurrentPizza).getPizzaIngredients()[i] = 0;
                return;
            }
        }
        System.out.println("You entered number not from the list. Please repeat command.");
    }

    public void showListOfIngredients(int indexOfCurrentPizza) {
        int positionInList = 0;
        int IndexOfArray = 0;
        while (IndexOfArray < 8) {
            if (pizza.get(indexOfCurrentPizza).getPizzaIngredients()[IndexOfArray] == 0) {
                System.out.println(++positionInList + ") " + ARRAYOFINGREDIENTS[IndexOfArray] +
                        " - " + ARRAYOFINGREDIENTSPRICES[IndexOfArray++] + " €");
            } else {
                IndexOfArray++;
            }
        }
    }

    public void showListOfCurrentIngredients(int indexOfCurrentPizza) {
        int positionInList = 0;
        int IndexOfArray = 0;
        while (IndexOfArray < 8) {
            if (pizza.get(indexOfCurrentPizza).getPizzaIngredients()[IndexOfArray] == 1) {
                System.out.println(++positionInList + ") " + ARRAYOFINGREDIENTS[IndexOfArray] +
                        " - " + ARRAYOFINGREDIENTSPRICES[IndexOfArray++] + " €");
            } else {
                IndexOfArray++;
            }
        }
    }

    public double getPriceOfPizza(int pizzaIndex) {
        double pizzaPrice = ((pizza.get(pizzaIndex).getPizzaType() == 1) ? 1.50 : 1.00);
        for (int i = 0; i < 8; i++){
            if (pizza.get(pizzaIndex).getPizzaIngredients()[i] == 1){
                pizzaPrice += ARRAYOFINGREDIENTSPRICES[i];
            }
        }
        return pizzaPrice;
    }

    public void showPizzaAttributes() {
        for (Pizza objectOfArray : pizza) {
            System.out.println("[" + orderNumber + " : " + clientNumber + " : " + objectOfArray.getPizzaName() + " : " + objectOfArray.getPizzaAmount() + "]");
        }
    }

    public void showPizzaAttributes(int indexOfPizza) {
        System.out.println("Name - " + pizza.get(indexOfPizza).getPizzaName() + "\nType - " +
                ((pizza.get(indexOfPizza).getPizzaType() == 1) ? "Calzone" : "Base") +
                "\nAmount - " + pizza.get(indexOfPizza).getPizzaAmount());
    }

    public String toString() {
        if (pizza.size() == 0)
        {
            return "There is no any pizza.\nOrder will be terminated";
        }
        String fullOrder = "\n********************************\nOrder: " + orderNumber + "\nClient: " + clientNumber;
        double TotalPrice = 0;
        for (int i = 0; i < pizza.size(); i++) {
            fullOrder += "\nName : " + pizza.get(i).getPizzaName() + "\n--------------------------------\n" +
                    "Pizza Base (Calzone)      " + ((pizza.get(i).getPizzaType() == 1) ? "1,50 €\n" : "1,00 €\n");
            for (int j = 0; j < 8; j++) {
                if (pizza.get(i).getPizzaIngredients()[j] == 1) {
                    fullOrder += String.format("%-14s %15.2f € %n", ARRAYOFINGREDIENTS[j], ARRAYOFINGREDIENTSPRICES[j]);
                }
            }
            fullOrder += "--------------------------------\n" + String.format("%-14s %15.2f € %n","Price: ",getPriceOfPizza(i))
                    + String.format("%-14s %15d %n","Amount: ", pizza.get(i).getPizzaAmount()) + "--------------------------------";
            TotalPrice += getPriceOfPizza(i) * pizza.get(i).getPizzaAmount();
        }
        fullOrder += String.format("%n%-14s %15.2f € %n","Total price: ",TotalPrice) + "******************************** ";
        return fullOrder;
    }

    public static int getOrderNumber() {
        return orderNumber;
    }

    public int getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(int clientNumber) {
        this.clientNumber = clientNumber;
    }

    public LocalTime getOrderTime() {
        return orderTime;
    }

    public ArrayList<Pizza> getPizza() {
        return pizza;
    }

    public void setPizza(ArrayList<Pizza> pizza) {
        this.pizza = pizza;
    }
}
