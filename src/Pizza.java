public class Pizza {
    private String pizzaName;
    private int[] pizzaIngredients = new int[8];
    private int pizzaType;
    private int pizzaAmount;

    public Pizza(String pizzaName, int pizzaType, int pizzaAmount) {
        this.pizzaName = pizzaName;
        this.pizzaType = pizzaType;
        this.pizzaAmount = pizzaAmount;
    }

    public String getPizzaName() {
        return pizzaName;
    }

    public void setPizzaName(String pizzaName) {
        this.pizzaName = pizzaName;
    }

    public int[] getPizzaIngredients() {
        return pizzaIngredients;
    }

    public void setPizzaIngredients(int[] pizzaIngredients) {
        this.pizzaIngredients = pizzaIngredients;
    }

    public int getPizzaType() {
        return pizzaType;
    }

    public void setPizzaType(int pizzaType) {
        this.pizzaType = pizzaType;
    }

    public int getPizzaAmount() {
        return pizzaAmount;
    }

    public void setPizzaAmount(int pizzaAmount) {
        this.pizzaAmount = pizzaAmount;
    }


}
