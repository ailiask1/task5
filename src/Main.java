public class Main {

    public static void main(String[] args) {
        Order[] orders = new Order[100];
        orders[0] = new Order(4000);
        System.out.println(orders[0].getOrderTime());
        orders[0].addPizza("Margarita", 1, 2);
        orders[0].addPizza("Pepperoni", 0, 3);
        System.out.println();
        orders[0].showPizzaAttributes();
        System.out.println();
        System.out.println(orders[0].toString());
        orders[1] = new Order(4372);
        orders[1].addPizza("Base ZZ", 0, 12);
        orders[1].showPizzaAttributes();
    }
}
